/* ========================================================================
 * PROJECT
 * ======================================================================== */

// Import dependencies
import 'jquery';

// Import components
import './components/nav';
import './components/search';

// Router
// Used to dynamically initialized Views based on the current page
import './router';