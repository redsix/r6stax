<?php

/**
 * Theme filters.
 */

namespace App;


/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    // Add page slug if it doesn't exist
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    // Remove unnecessary classes
    $home_id_class = 'page-id-' . get_option('page_on_front');
    $remove_classes = array(
        'page-template-default',
        'page-template',
        $home_id_class
    );
    $classes = array_diff( $classes, $remove_classes );
    $classes = str_replace( 'template-template', 'template', $classes );

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});


/**
 * Set a custom excerpt length
 */
add_filter('excerpt_length', function ($length) {
    return 30;
});


// Sanitize the filename of the uploaded files to remove accents and special
// characters
add_filter('sanitize_file_name', function ($filename) {
    return remove_accents( $filename );
});


/**
 * Allow upload of SVG in the media menu of WordPress
 * @return null
 */
add_filter( 'upload_mimes', function ($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
});


/**
 * The sage/display_sidebar filter can be used to define which conditions
 * to enable the primary sidebar on.
 */
add_filter( 'sage/display_sidebar', function ($display) {
    static $display;
    $get_page_template = basename( get_page_template() );

    isset($display) || $display = in_array(true, [
        $get_page_template == 'template-sidebar.blade.php',
    ]);

    return $display;
});


/**
 * Remove the wrapper around the inputs generated automatically by Contact Form 7
 */
add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

    return $content;
});


/**
 * Add "… Continued" to the excerpt.
 *
 * @return string
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});