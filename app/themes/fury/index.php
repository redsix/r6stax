<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php wp_head(); ?>

        <link rel="apple-touch-icon" sizes="180x180" href="{!! get_stylesheet_directory_uri() !!}/assets/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="{!! get_stylesheet_directory_uri() !!}/assets/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="{!! get_stylesheet_directory_uri() !!}/assets/favicons/favicon-16x16.png">
        <link rel="manifest" href="{!! get_stylesheet_directory_uri() !!}/assets/favicons/site.webmanifest">
        <link rel="mask-icon" href="{!! get_stylesheet_directory_uri() !!}/assets/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="{!! get_stylesheet_directory_uri() !!}/assets/favicons/favicon.ico">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-config" content="{!! get_stylesheet_directory_uri() !!}/assets/favicons/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
    </head>

    <body <?php body_class(); ?>>
        <?php wp_body_open(); ?>
        <?php do_action('get_header'); ?>

        <div id="app">
            <?php echo \Roots\view(\Roots\app('sage.view'), \Roots\app('sage.data'))->render(); ?>
        </div>

        <?php do_action('get_footer'); ?>
        <?php wp_footer(); ?>
    </body>
</html>