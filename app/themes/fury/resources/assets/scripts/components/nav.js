$(() => {
    // Cache the mobile navigation button
    let $mobileNavBtn = $( '#navToggle' );

    $mobileNavBtn.on( 'click', function( e ) {
        e.preventDefault();
        document.body.classList.toggle( 'mobile-nav-opened' );
    });

    // Bind an event on keyup to detect the Escape key.
    // If pressed while the navigation menu is opened, close it
    $( document ).on( 'keyup', function( e ) {
        if ( e.keyCode === 27 && document.body.classList.contains('mobile-nav-opened') ) {
            $mobileNavBtn.trigger( 'click' );
        }
    });

    // When the user is resizing the window, prevent the main navigation
    let resizeTimer;
    $( window ).on('resize', function() {
        document.body.classList.add( 'resizing' );
        clearTimeout( resizeTimer );
        resizeTimer = setTimeout(function() {
            document.body.classList.remove( 'resizing' );
        }, 250);
    });
});