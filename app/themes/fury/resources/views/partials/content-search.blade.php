<div class="content">
    @php($posts = App::posts())
    @foreach($posts->posts as $post)
        @include('blocks.post-preview', $post)
    @endforeach
</div>