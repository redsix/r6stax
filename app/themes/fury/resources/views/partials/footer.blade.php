<footer class="site-footer">
    <div class="wrapper">
        @php(dynamic_sidebar('sidebar-footer'))

        @if ($theme_options->copyrights)
            {{ $theme_options->copyrights }}
        @else
            <p class="copyright">&copy; {{ date('Y') }} {{ $siteName }} {{ __('- All rights reserved', 'theme') }}</p>
        @endif
    </div>
</footer>