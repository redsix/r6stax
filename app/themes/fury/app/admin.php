<?php

/**
 * Theme admin.
 */

namespace App;

use WP_Customize_Manager;

use function Roots\asset;


/**
 * Remove the default "Hello Dolly" plugin that comes pre-installed with WP
 * @return none
 */
add_action('admin_init', function () {
    if (file_exists(WP_PLUGIN_DIR.'/hello.php')) {
        require_once(ABSPATH.'wp-admin/includes/plugin.php');
        require_once(ABSPATH.'wp-admin/includes/file.php');
        delete_plugins(array('hello.php'));
    }
});


// Hide the WP Admin Bar on the public site when logged in
add_filter( 'show_admin_bar', '__return_false' );


// Disable Theme Editor
add_action('_admin_menu', function () {
	remove_menu_page( 'edit-comments.php' );
	remove_action('admin_menu', '_add_themes_utility_last', 101);
});


/**
 * Register the Options page in the Admin
 */
add_action('init', function () {
    if ( function_exists('acf_add_options_page') ) {
        acf_add_options_page( array(
            'page_title'    => __('Theme Options', 'theme'),
            'menu_title'    => __('Theme Options', 'theme'),
            'menu_slug'     => 'theme_options',
            'parent_slug'   => '',
            'capability'    => 'manage_options',
            'position'      => 79,
            'icon_url'      => 'dashicons-admin-generic',
        ));
    }
});

/**
 * Register the customizer assets.
 *
 * @return void
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset('scripts/customizer.js')->uri(), ['customize-preview'], null, true);
});