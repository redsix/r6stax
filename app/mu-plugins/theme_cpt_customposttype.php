<?php

// Simple WordPress custom post types.
// https://github.com/jjgrainger/PostTypes
use PostTypes\PostType;
use PostTypes\Taxonomy;


// Create the custom post type with a set of labels and options
$names = [
    'name'          => 'posttype',
    'singular'      => __('Post Type', 'theme'),
    'plural'        => __('Post Types', 'theme'),
    'slug'          => 'post-type'
];
$options = [
    'has_archive'   => false,
    'supports' 		=> array('title', 'thumbnail', 'editor', 'page-attributes', 'excerpt'),
];

$books = new PostType($names, $options);

// Add the custom taxonomy
$books->taxonomy('tax');

// Hide the date and author columns
$books->columns()->hide(['date', 'author']);

// Add custom columns
$books->columns()->add([
    //'rating' => __('Rating'),
]);

// Populate the custom column
// $books->columns()->populate('rating', function($column, $post_id) {
//    echo get_post_meta($post_id, 'rating') . '/10';
// });


// Set sortable columns
$books->columns()->sortable([
    // 'price' => ['price', true],
]);

// Set the Books menu icon
$books->icon('dashicons-book-alt');

// Register the PostType to WordPress
$books->register();

// Create the genre Taxonomy
$names = [
    'name' => 'tax',
    'singular' => 'Taxonomy',
    'plural' => 'Taxonomies',
    'slug' => 'tax'
];

$taxonomy = new Taxonomy($names);

// Add a popularity column to the genre taxonomy
$taxonomy->columns()->add([
    // 'popularity' => 'Popularity'
]);

// Populate the new column
/*
$taxonomy->columns()->populate('popularity', function($content, $column, $term_id) {
    return get_term_meta($term_id, 'popularity', true);
});
*/

// Register the taxonomy to WordPress
$taxonomy->register();
