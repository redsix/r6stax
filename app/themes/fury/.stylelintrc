{
    "ignoreFiles": [],
    "plugins": [
        "stylelint-order"
    ],
    "extends": "stylelint-config-standard",
    "rules": {
        "font-family-name-quotes": "always-unless-keyword",
        "function-url-quotes": "always",
        "selector-attribute-quotes": "always",
        "string-quotes": "double",

        "selector-max-compound-selectors": 3,
        "selector-max-specificity": "1,4,3",
        "selector-max-class": 3,
        "max-nesting-depth": [
            5,
            {"ignore": ["pseudo-classes", "blockless-at-rules"]},
            {"ignoreAtRules": ["media", "include"]}
        ],

        "color-no-invalid-hex": true,
        "color-hex-case": "lower",
        "color-hex-length": "short",
        "selector-pseudo-element-colon-notation": "single",
        "shorthand-property-no-redundant-values": true,

        "comment-whitespace-inside": "always",
        "function-whitespace-after": "always",
        "indentation": [4, {"severity": "warning"}],
        "max-empty-lines": 2,
        "no-empty-source": null,
        "block-no-empty": null,
        "declaration-block-semicolon-newline-after": "always",
        "no-descending-specificity": null,
        "block-opening-brace-space-before": "always-multi-line",
        "block-closing-brace-newline-after": "always",
        "value-list-comma-newline-after": "never-multi-line",
        "value-list-comma-newline-before": "never-multi-line",
        "value-list-comma-space-after": "always",
        "value-list-comma-space-before": "never",

        "selector-type-no-unknown": [
            true,
            {
                "severity": "warning",
                "ignoreNamespaces": ["/^mat-/"]
            }
        ],
        "selector-pseudo-element-no-unknown": [
            true,
            {
                "severity": "warning",
                "ignorePseudoElements": [":ng-deep"]
            }
        ],
        "at-rule-no-unknown": null,
        "media-feature-colon-space-before": "never",
        "media-feature-colon-space-after": "always",
        "media-feature-range-operator-space-after": "always",
        "media-feature-range-operator-space-before": "always",

        "order/order": [
            "dollar-variables",
            "at-variables",
            {
				"type": "at-rule",
				"name": "extend"
			},
			{
				"type": "at-rule",
				"name": "include",
				"hasBlock": false
			},
			{
				"type": "at-rule",
				"name": "mixin",
				"hasBlock": false
			},
            "declarations",
			{
				"type": "at-rule",
				"name": "include",
				"hasBlock": true
			},
			"rules"
        ],
        "order/properties-order": [
            {
                groupName: "dimensions",
                emptyLineBefore: "never",
                properties: [
                    "width",
                    "height",
                ],
            },
        ],
    },
}