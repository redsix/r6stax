![Red Six Banner](https://dl.dropboxusercontent.com/s/rd61rjsva4q3vtd/banner595x180.png)

## Project

The Red Six "stax" is an open-source WordPress boilerplate that includes a wide set of modern tools and technologies to jumpstart your projects and make development fun.

It is based on the Sage 9 WordPress starter theme and the Bedrock boilerplate for WordPress, and includes:

1. A set of configuration files that make it easier to retrieve dependencies and setup deployments
2. The WordPress core folder
3. An application folder composed of the website's content and the WordPress starter theme


Here are some of the benefits of this stack:

+ "Template inheritence with the theme wrapper. Markup is handled by one file instead of being scattered across all template files like typical themes in order to stay DRY"
+ Separate folders for WordPress core files and the site's logic for a cleaner structure and avoid cluttering up the root directory
+ Environment specific configuration files and environment variables with Dotenv
+ Dependencies management using Composer & yarn
+ Multilingual ready: the starter theme is WPML compatible
+ Automatic compilation of the Sass files and check for JavaScript/PHP errors using webpack build process
+ Template markup based on HTML5 Boilerplate for better accessibility and development of compliant websites

If you'd like to contribute or use it please feel free to do so!
The Red Six team =)


## About Us

Red Six is an independent technology-driven creative studio, with a futuristic vision, that designs and develops digital web and mobile solutions.

We aim to redefine standards and close the technological gap by developing websites based on smart code, creative designs, and an unforgettable experience for your users.

## WordPress Theme

![Fury Screenshot](https://dl.dropboxusercontent.com/s/09o2361t6u2v1ah/screenshot.png =260x)

## Technologies, Frameworks & Tools

* [WordPress](https://wordpress.com/)
* [Bedrock](http://roots.io/wordpress-stack/) & [Sage](https://roots.io/sage/)
* [node.js](http://nodejs.org/)
* [Composer](https://getcomposer.org/)
* [Sass](http://sass-lang.com/)
* [Webpack](https://webpack.js.org/)
* [Browsersync](https://browsersync.io/)
* [HTML5Boilerplate](http://html5boilerplate.com/)


## Required Plugins & Resources
These plugins and resources are required automatically by Composer in each new projects. We try to keep it lean and

* [WP Migrate DB Pro](https://deliciousbrains.com/wp-migrate-db-pro/)
* [Contact Form 7](http://wordpress.org/plugins/contact-form-7/)
* [WordPress SEO by Yoast](https://wordpress.org/plugins/wordpress-seo/)
* [Advanced Custom Fields Pro](https://wordpress.org/plugins/advanced-custom-fields/)
* [Yoast SEO](https://wordpress.org/plugins/wordpress-seo/)
* [Akismet](https://wordpress.org/plugins/akismet/)
* [Smush Image Compression and Optimization](https://wordpress.org/plugins/wp-smushit/)


## Installation

1. Clone the project to your computer

2. To install the project's core files (WordPress + plugins) simply run Composer at the root of your project folder. This will automatically install the dependencies and run the initialization script, which will create the `.env` file

    `composer install`

3. Enter your database and project info in the `.env` file. You can also generate the salts keys at this time

4. To install the theme's backend/frontend dependencies simply navigate to the theme's folder and run the commands:

    `composer install`
    `yarn`

5. Update `resources/assets/config.json` settings:

    - `devUrl` should reflect your local development hostname
    - `publicPath` should reflect your WordPress folder structure


6. Then to compile all the front-end resources (CSS, JavaScript, images, fonts, etc.) simply run the task runner with the command:

    `yarn run build`

    It is also possible to start the watch process of the Task Runner for local developement by running:

    `yarn run start`


##Build commands

    yarn start — Compile assets when file changes are made, start Browsersync session
    yarn build — Compile and optimize the files in your assets directory
    yarn build:production — Compile assets for production



##Files hierarchy:

```
root/
├── app/							# →
│   ├── mu-plugins/					# → must-use plugins, including the custom post types and Dapper
│   ├── plugins/ 					# → WordPress default plugins folder
│   ├── themes/						# → WordPress default themes folder
│   |   ├── app/					# → the controllers for the project
│   |   ├── config/					# → configuration files for the theme
│   |   ├── dist/					# → processed front-end assets ready for production
│   |   ├── resources/				# →
│   |   |   ├── assets/				# → front-end assets including the JavaScript, Sass files, build configuration, favicons and images
│   |   |   ├── views/ 				# → views folder including the partials, page build sections, UI elements and WordPress templates
│   |   |   ├── functions.php		# →
│   |   |   ├── index.php			# → The theme's entry point
│   |   |   ├── screenshot.png 		# → The theme's image displayed in the "Appearance" menu in the backend
│   |   |   └── style.css			# → The WordPress required file to create a theme
│   |   ├── .eslintrc.js			# → ESLint Configuration file
│   |   ├── composer.json			# →
│   |   ├── composer.lock			# →
│   |   └── package.json			# →
│   ├── upgrade/ 					# → WordPress default upgrade placeholder folder
│   └── uploads/ 					# → WordPress default file upload folder
├── config/ 						# → environment specific configuration
├── vendor/ 						# → dependencies to load Composer and phpdotenv
├── wp/								# → The WordPress core files
├── .editorconfig					# → EditorConfig configuration file for the different IDEs
├── .env 							# → contains the site's configuration, including database information, site's urls, environment, salts
├── .htaccess 						# → configuration file for Apache on the web server
├── .htpasswd 						# → password protection for the staging environment
├── index.php 						# → entry point to load WordPress
├── wp-cli.yml						# → let the server know where to load WordPress from
└── wp-config.php 					# → reference to the vendors and CMS configuration
```


## Contributors

The Red Six squad