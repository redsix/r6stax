@extends('layouts.app')

@section('content')
    @include('partials.page-header')

    @if (!have_posts())
        <div class="content container">
            <div class="row no-gutters">
                <div class="col-12">
                    <p>{{ __('Sorry, but the page you were trying to view does not exist.', 'theme') }}</p>
                    <p><a href="<?php echo esc_url(home_url('/')); ?>">{{ __('Back to Homepage', 'theme') }}</a></p>
                </div>
            </div>
        </div>
    @endif
@endsection
