<article class="post-preview">
    <a class="post-image" href="{{ the_permalink() }}" @if(has_post_thumbnail()) App::get_featured_image() @endif;></a>

    <div class="post-content">
        @include('partials/entry-meta')

        <h2 class="post-title">
            <a href="{{ the_permalink() }}">{{ the_title() }}</a>
        </h2>

        {{ the_excerpt() }}

        <a class="lnk-more" href="{{ the_permalink() }}">{{ __('Continue reading', 'theme') }}&nbsp;»</a>
    </div>
</article>