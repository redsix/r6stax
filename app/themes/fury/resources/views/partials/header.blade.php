<header class="site-header">
    <div class="wrapper">
        <a class="logo-main" href="{{ home_url('/') }}">{{ $siteName }}</a>

        <nav class="nav-main">
            @if (has_nav_menu('primary_navigation'))
                {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new App\NavWalker(), 'menu_class' => 'nav', 'echo' => false]) !!}
            @endif
        </nav>

        <button id="navToggle" class="burger mobile-only" type="button">
            <span></span>
            <span></span>
            <span></span>
        </button>
    </div>
</header>