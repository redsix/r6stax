<?php

/**
 * Return WP Posts
 *
 *
 * @param $_POST['nbPosts'] is the number of posts to return. If not
 * specified will use the WP default setting
 * @param $_POST['page'] is the current page to return
 *
 * @return HTML of the posts usin the views/post-preview.php template
 */
add_action( 'wp_ajax_load_posts', 'load_posts' );
add_action( 'wp_ajax_nopriv_load_posts', 'load_posts' );
function load_posts() {
    $args = array();

    // Number of posts to get
    // Check if a param was passed otherwise get the default option
    $posts_per_page = get_option('posts_per_page');
    if ( isset($_POST['posts_per_page']) ) {
        $posts_per_page = $_POST['posts_per_page'];
    }

    $post_type = 'post';
    if ( isset($_POST['post_type']) ) {
        $post_type = $_POST['post_type'];
    }

    // Default options for the WP Query
    $args = array(
        'posts_per_page'    => $posts_per_page,
        'post_type'         => $post_type,
        'post_status'       => 'publish',
    );

    // The page to get the posts from
    if ( isset($_POST['page']) && $_POST['page'] != '' ) {
        $args['paged'] = $_POST['page'];
    }

    // Search query
    if ( isset($_POST['s']) && $_POST['s'] != '' ) {
        $args['s'] = $_POST['s'];
    }

    // The category for the posts
    if ( isset($_POST['cat']) && $_POST['cat'] != '' ) {
        $args['cat'] = $_POST['cat'];
    }

    // The date params (month or year)
    if ( isset($_POST['monthnum']) ) {
        $args['monthnum'] = $_POST['monthnum'];
    }
    if ( isset($_POST['year']) ) {
        $args['year'] = $_POST['year'];
    }

    $the_query = new WP_Query( $args );

    if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();

            echo App\template( 'blocks.post-preview', $data );
        }

        wp_reset_postdata();
    } else {
        echo '<p class="no-results">' . __('Nothing found for this query', 'theme') . '</p>';
    }

    wp_die();
}
