<?php

namespace App\View\Composers;

use Roots\Acorn\View\Composer;
use WP_Query;

class App extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        '*',
    ];


    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'siteName' => $this->siteName(),
            'background' => $this->background(),
            'options' => $this->options(),
            'socialLinks' => $this->social_links()
        ];
    }


    /**
     * Returns the site name.
     *
     * @return string
     */
    public function siteName()
    {
        return get_bloginfo('name', 'display');
    }

    /**
     * Returns inline CSS containing the background-image of a post using
     * the featured image's url and using the dimensions "background"
     *
     * @return void
     */
    public static function background()
    {
        $featured_img_id = get_post_thumbnail_id();
        $featured_img_array = wp_get_attachment_image_src( $featured_img_id, 'background', true );
        $bg_url = '';
        if ( $featured_img_array ) $bg_url = $featured_img_array[0];

        return 'style="background-image: url(' . $bg_url . ');"';
    }


    /**
     * Theme Options
     * Contains the ACF fields created in the Theme Options and that will be
     * used throughout the site
     *
     * @return object
     */
    public function options()
    {
        return (object) array(
            'copyrights' => get_field('copyrights', 'option'),
        );
    }


    /**
     * Social Media Links
     * Contains the ACF fields created in the Theme Options and that will be
     * used throughout the site
     *
     * @return object
     */
    public function social_links()
    {
        return (object) array(
            'twitter' => get_field('twitter', 'option'),
            'facebook' => get_field('facebook', 'option'),
        );
    }
}
