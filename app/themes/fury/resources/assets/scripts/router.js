// Page-specific scripts to initialize
import page_home from './pages/Home';

// Load Events
$(() => {
    if ( document.body.classList.contains('home') ) {
        page_home.initialize();
    }
});